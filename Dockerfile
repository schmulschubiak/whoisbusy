FROM node:21-alpine as whoisbusy-server
RUN mkdir -p /tmp/whoisbusy-server/node_modules && chown -R node:node /tmp/whoisbusy-server
WORKDIR /tmp/whoisbusy-server
COPY --chown=node:node . .
RUN npm install
RUN npm run build
RUN mv /tmp/whoisbusy-server/.output /srv/whoisbusy
RUN chown -R node:node /srv/whoisbusy
RUN find /tmp/whoisbusy-server -delete
WORKDIR /srv/whoisbusy
EXPOSE 3000
CMD ["node", "server/index.mjs"]