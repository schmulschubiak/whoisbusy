import { User, useStorage } from "#imports";
import { userRequest } from "~/utils/typings";

export default defineEventHandler(async (event) => {
  const body = await readValidatedBody(event, userRequest.safeParse);

  if (body.success) {
    const userState = body.data;

    const userId = userState.id.toUpperCase();

    if (await useStorage("users").hasItem(userId)) {
      let user = await useStorage("users").getItem<User>(userId);
      user.status = userState.status;
      user.lastUpdated = new Date();
      await useStorage("users").setItem(userId, user);
    } else {
      let user: User = {
        id: userId,
        status: userState.status,
        lastUpdated: new Date(),
      };
      await useStorage("users").setItem(userId, user);
    }
        
  } else {
    throw createError({
      statusCode: 422,
      statusMessage: "Invalid request body",
    });
  }
});
