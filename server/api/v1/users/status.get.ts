import { getValidatedQuery, Status, User } from "#imports";
import { userArrayQuery, userSingleQuery } from "~/utils/typings";

export default defineEventHandler(async (event) => {
  const multiUserQuery = await getValidatedQuery(
    event,
    userArrayQuery.safeParse,
  );

  const singleUserQuery = await getValidatedQuery(
    event,
    userSingleQuery.safeParse,
  );

  if (!multiUserQuery.success && !singleUserQuery.success) {
    if (!multiUserQuery.success) {
      throw createError({
        statusCode: 422,
        statusMessage:
          "Invalid request multiUserQuery param, needs ?withUserId=[UUID]&withUserId=[UUID]",
      });
    } else {
      throw createError({
        statusCode: 422,
        statusMessage:
          "Invalid request singleUserQuery param, needs ?withUserId=[UUID]",
      });
    }
  }

  const requestedUser = multiUserQuery.success
    ? multiUserQuery.data.withUserId
    : singleUserQuery.success
      ? [singleUserQuery.data.withUserId]
      : [];

  const userStats: User[] = await Promise.all(
    requestedUser.map(async (userId): Promise<User> => {
      return await useStorage("users").getItem(userId.toUpperCase());
    }),
  );

  const now = new Date();
  const lastMinute = new Date(now.getTime() - 60 * 1000);

  const usersActiveInLast5Minutes = userStats
    .filter((u) => !!u)
    .filter((u) => new Date(u.lastUpdated) > lastMinute);

  if(usersActiveInLast5Minutes.length === 0) {
    return Status.OFFLINE;
  }

  const areSomeUsersBusyInLast5Minutes = usersActiveInLast5Minutes.some(
    (u) => u.status === Status.DONOTDISTURB,
  );

  return areSomeUsersBusyInLast5Minutes
    ? Status.DONOTDISTURB
    : Status.AVAILABLE;
});
