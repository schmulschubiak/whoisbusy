import { z } from "zod";
import { getValues } from "~/utils/enums";

export class User {
  id: string;
  status: Status;
  lastUpdated: Date;
}

export const Status = {
  AVAILABLE: "AVAILABLE",
  ABSENT: "ABSENT",
  DONOTDISTURB: "DONOTDISTURB",
  OFFLINE: "OFFLINE",
  NEWACTIVITY: "NEWACTIVITY",
  UNKNOWN: "UNKNOWN",
};

export type Status = (typeof Status)[keyof typeof Status];

export const userRequest = z.object({
  id: z.string().uuid("Invalid user ID, must be a valid UUID"),
  status: z.enum(getValues(Status)),
});

export const userSingleQuery = z.object({
  withUserId: z.string().uuid("Invalid user ID, must be a valid UUID"),
});

export const userArrayQuery = z.object({
  withUserId: z.array(z.string().uuid("Invalid user ID, must be a valid UUID")),
});