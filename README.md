# WhoIsBusy

Status server for LookImBusy BusyLight

## API

### PUT
- `/api/v1/users/status`
- body: 
  ```json
  {
    "status": "AVAILABLE",
    "userId": "da620194-51d8-4f5c-a190-81af22409263"
  }
  ```
  - status: "AVAILABLE" | "ABSENT" | "DONOTDISTURB" | "OFFLINE" | "NEWACTIVITY" | "UNKNOWN"
  - userId: UUID   

### GET 
- `/api/v1/users/status`
  - Query-Param: withUserId
    - `?withUserId=da620194-51d8-4f5c-a190-81af22409263`
    - Multilpe userIds can be passed with multiple `withUserId` query params
    - `?withUserId=da620194-51d8-4f5c-a190-81af22409263&withUserId=da620194-51d8-4f5c-a190-81af22409263`
  - Response:
    - "AVAILABLE" | "DONOTDISTURB"
    - "OFFLINE" if no users are found 

